﻿using MVCAuthentication.Controllers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nunit_TestEnv
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void ShouldDepartmentIndex()
        {
            var obj = new AccountController();
            var actResult = obj.Register() as ViewResult;
            //actResult.ViewName = "Index";

            Assert.That(actResult.ViewName, Is.EqualTo("Index"));
        }

        
    }
}
